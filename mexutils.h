// mexutils.h

#include <gsl/gsl_matrix.h>
#include "mex.h"

/* Convert a mxArray to a gsl_matrix */
gsl_matrix * mxmat_to_gslmat (const mxArray * mxmat);

/* Convert a mxArray to a gsl_matrix */
gsl_vector * mxmat_to_gslvec (const mxArray * mxmat);

/* Convert a gsl_matrix to a mxArray */
mxArray * gslmat_to_mxmat (const gsl_matrix * gslmat);

/* Convert a gsl_vector to a mxArray */
mxArray * gslvec_to_mxmat (const gsl_vector * gslvec);
