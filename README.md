# mex-gsl-utils

Utility functions to work with GNU GSL and Matlab Executables (MEX), in C or C++ code. (Need these libraries to be installed.)

These simple functions allow the conversion of GSL matrices and vectors to MEX arrays, and vice versa. A deep copy is done, without modifying the input object.

Example of use: [sparse-nmf](https://gitlab.com/nnadisic/sparse-nmf/blob/master/src/mex/arborescentMex.cc)
